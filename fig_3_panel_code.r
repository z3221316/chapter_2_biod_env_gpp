#######################################################################################################################
## Continental GAM plots (figure_2 in manuscript)

## setup
setwd("E:/GPP_Beta_analysis/R")
library(Cairo)
library(mgcv)
data <- read.csv("0112_IBRA_KAV.csv", stringsAsFactors=FALSE)
R1 = data



#####################################################################################################################
## AV plots

fit1av = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log)) 
testdata1av = data.frame(b10km=mean(fit1av$model$b10km),
                      temp_10kav=mean(fit1av$model$temp_10kav),
                      rain_10kav=seq(min(R1[,"rain_10kav"]),max(R1[,"rain_10kav"]), length=length(R1$gpp_10kav)))
pred_GPP1av = predict(fit1av, newdata=testdata1av, type='response')

fitRav = gam(gpp_10kav~s(rain_10kav,k=5),data=R1, family=Gamma(link=log))
testdataRav = data.frame(rain_10kav=seq(min(R1[,"rain_10kav"]),max(R1[,"rain_10kav"]), length=length(R1$gpp_10kav)))
pred_GPPRav = predict(fitRav, newdata=testdataRav, type='response')

fit2av = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log)) 
testdata2av = data.frame(b10km=mean(fit2av$model$b10km),
                      rain_10kav=mean(fit2av$model$rain_10kav),
                      temp_10kav=seq(min(R1[,"temp_10kav"]),max(R1[,"temp_10kav"]), length=length(R1$gpp_10kav)))
pred_GPP2av = predict(fit2av, newdata=testdata2av, type='response')

fitTav = gam(gpp_10kav~s(temp_10kav,k=5),data=R1, family=Gamma(link=log))
testdataTav = data.frame(temp_10kav=seq(min(R1[,"temp_10kav"]),max(R1[,"temp_10kav"]), length=length(R1$gpp_10kav)))
pred_GPPTav = predict(fitTav, newdata=testdataTav, type='response')

fit3av = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(alpha,k=5), data=R1, family=Gamma(link=log))
testdata3av = data.frame(rain_10kav=mean(fit3av$model$rain_10kav),
                      temp_10kav=mean(fit3av$model$temp_10kav),
                      alpha=seq(min(R1[,"alpha"]),max(R1[,"alpha"]), length=length(R1$gpp_10kav)))
pred_GPP3av = predict(fit3av, newdata=testdata3av, type='response')

fitAav = gam(gpp_10kav~s(alpha,k=5),data=R1, family=Gamma(link=log))
testdataAav = data.frame(alpha=seq(min(R1[,"alpha"]),max(R1[,"alpha"]), length=length(R1$gpp_10kav)))
pred_GPPAav = predict(fitAav, newdata=testdataAav, type='response')

fit4av = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log))
testdata4av = data.frame(rain_10kav=mean(fit4av$model$rain_10kav),
                      temp_10kav=mean(fit4av$model$temp_10kav),
                      b10km=seq(min(R1[,"b10km"]),max(R1[,"b10km"]), length=length(R1$gpp_10kav)))
pred_GPP4av = predict(fit4av, newdata=testdata4av, type='response')

fitBav = gam(gpp_10kav~s(b10km,k=5),data=R1, family=Gamma(link=log))
testdataBav = data.frame(b10km=seq(min(R1[,"b10km"]),max(R1[,"b10km"]), length=length(R1$gpp_10kav)))
pred_GPPBav = predict(fitBav, newdata=testdataBav, type='response')

#####################################################################################################################
## CV plots

fit1cv = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log)) 
testdata1cv = data.frame(b10km=mean(fit1cv$model$b10km),
                      temp_10kcv=mean(fit1cv$model$temp_10kcv),
                      rain_10kcv=seq(min(R1[,"rain_10kcv"]),max(R1[,"rain_10kcv"]), length=length(R1$gpp_10kcv)))
pred_GPP1cv = predict(fit1cv, newdata=testdata1cv, type='response')

fitRcv = gam(gpp_10kcv~s(rain_10kcv,k=5),data=R1, family=Gamma(link=log))
testdataRcv = data.frame(rain_10kcv=seq(min(R1[,"rain_10kcv"]),max(R1[,"rain_10kcv"]), length=length(R1$gpp_10kcv)))
pred_GPPRcv = predict(fitRcv, newdata=testdataRcv, type='response')

fit2cv = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log)) 
testdata2cv = data.frame(b10km=mean(fit2cv$model$b10km),
                      rain_10kcv=mean(fit2cv$model$rain_10kcv),
                      temp_10kcv=seq(min(R1[,"temp_10kcv"]),max(R1[,"temp_10kcv"]), length=length(R1$gpp_10kcv)))
pred_GPP2cv = predict(fit2cv, newdata=testdata2cv, type='response')

fitTcv = gam(gpp_10kcv~s(temp_10kcv,k=5),data=R1, family=Gamma(link=log))
testdataTcv = data.frame(temp_10kcv=seq(min(R1[,"temp_10kcv"]),max(R1[,"temp_10kcv"]), length=length(R1$gpp_10kcv)))
pred_GPPTcv = predict(fitTcv, newdata=testdataTcv, type='response')

fit3cv = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(alpha,k=5), data=R1, family=Gamma(link=log))
testdata3cv = data.frame(rain_10kcv=mean(fit3cv$model$rain_10kcv),
                      temp_10kcv=mean(fit3cv$model$temp_10kcv),
                      alpha=seq(min(R1[,"alpha"]),max(R1[,"alpha"]), length=length(R1$gpp_10kcv)))
pred_GPP3cv = predict(fit3cv, newdata=testdata3cv, type='response')

fitAcv = gam(gpp_10kcv~s(alpha,k=5),data=R1, family=Gamma(link=log))
testdataAcv = data.frame(alpha=seq(min(R1[,"alpha"]),max(R1[,"alpha"]), length=length(R1$gpp_10kcv)))
pred_GPPAcv = predict(fitAcv, newdata=testdataAcv, type='response')

fit4cv = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log))
testdata4cv = data.frame(rain_10kcv=mean(fit4cv$model$rain_10kcv),
                      temp_10kcv=mean(fit4cv$model$temp_10kcv),
                      b10km=seq(min(R1[,"b10km"]),max(R1[,"b10km"]), length=length(R1$gpp_10kcv)))
pred_GPP4cv = predict(fit4cv, newdata=testdata4cv, type='response')

fitBcv = gam(gpp_10kcv~s(b10km,k=5),data=R1, family=Gamma(link=log))
testdataBcv = data.frame(b10km=seq(min(R1[,"b10km"]),max(R1[,"b10km"]), length=length(R1$gpp_10kcv)))
pred_GPPBcv = predict(fitBcv, newdata=testdataBcv, type='response')


#####################################################################################################################
## create one panel for AV and CV plots

CairoPNG(width = 12000, height = 19416, file = "GEB_FIG_3_revised.png", canvas="white", bg = "white", units="px", dpi=600)
par(mfrow=c(4,2), mar=c(10,10,10,10), mgp=c(7, 3, 0))
par(font.axis = 1, font.lab = 2)
smoothScatter(R1[,"rain_10kav"], R1[,"gpp_10kav"], nrpoints = 0, pch=19, col="blue", cex=2, cex.axis=4, cex.lab=5, xlab="monthly rainfall (mm)", ylab="GPP")
lines(testdataRav$rain_10kav, pred_GPPRav, col="green", lwd=5)
lines(testdata1av$rain_10kav, pred_GPP1av, col="orange", lwd=5)
par(font.axis = 1, font.lab = 2)
smoothScatter(R1[,"rain_10kcv"], R1[,"gpp_10kcv"], nrpoints = 0, pch=19, cex=2, cex.axis=4, cex.lab=5, xlab="CV rain", ylab="CV GPP")
lines(testdataRcv$rain_10kcv, pred_GPPRcv, col="green", lwd=5)
lines(testdata1cv$rain_10kcv, pred_GPP1cv, col="orange", lwd=5)
par(font.axis = 1, font.lab = 2)
smoothScatter(R1[,"temp_10kav"], R1[,"gpp_10kav"], nrpoints = 0, pch=19, cex=2, cex.axis=4, cex.lab=5, xlab="monthly max temp(�C)", ylab="GPP")
lines(testdataTav$temp_10kav,pred_GPPTav,col="green", lwd=5)
lines(testdata2av$temp_10kav, pred_GPP2av, col="orange", lwd=5)
par(font.axis = 1, font.lab = 2)
smoothScatter(R1[,"temp_10kcv"], R1[,"gpp_10kcv"], nrpoints = 0, pch=19, cex=2, cex.axis=4, cex.lab=5, xlab="CV temp", ylab="CV GPP")
lines(testdataTcv$temp_10kcv,pred_GPPTcv,col="green", lwd=5)
lines(testdata2cv$temp_10kcv, pred_GPP2cv, col="orange", lwd=5)
par(font.axis = 1, font.lab = 2)
smoothScatter(R1[,"alpha"], R1[,"gpp_10kav"], nrpoints = 0, pch=19, cex=2, cex.axis=4, cex.lab=5, xlab="alpha diversity", ylab="GPP")
lines(testdataAav$alpha, pred_GPPAav,col="green", lwd=5)
lines(testdata3av$alpha, pred_GPP3av, col="orange", lwd=5)
par(font.axis = 1, font.lab = 2)
smoothScatter(R1[,"alpha"], R1[,"gpp_10kcv"], nrpoints = 0, pch=19, cex=2, cex.axis=4, cex.lab=5, xlab="alpha diversity", ylab="CV GPP")
lines(testdataAcv$alpha, pred_GPPAcv,col="green", lwd=5)
lines(testdata3cv$alpha, pred_GPP3cv, col="orange", lwd=5)
par(font.axis = 1, font.lab = 2)
smoothScatter(R1[,"b10km"], R1[,"gpp_10kav"], nrpoints = 0, pch=19, cex=2, cex.axis=4, cex.lab=5, xlab="beta diversity", ylab="GPP")
lines(testdataBav$b10km, pred_GPPBav,col="green", lwd=5)
lines(testdata4av$b10km, pred_GPP4av, col="orange", lwd=5)
par(font.axis = 1, font.lab = 2)
smoothScatter(R1[,"b10km"], R1[,"gpp_10kcv"], nrpoints = 0, pch=19, cex=2, cex.axis=4, cex.lab=5, xlab="beta diversity", ylab="CV GPP")
lines(testdataBcv$b10km, pred_GPPBcv,col="green", lwd=5)
lines(testdata4cv$b10km, pred_GPP4cv, col="orange", lwd=5)
dev.off()

CairoPNG(width = 8000, height = 8000, file = "Fig_3_bold_test.png", canvas="white", bg = "white", units="px", dpi=600)
par(font.axis=2, font.lab=2, cex.lab=3, cex.axis=2.3, cex=2
smoothScatter(R1[,"b10km"], R1[,"gpp_10kcv"], nrpoints = 0, pch=19, xlab="β-diversity", ylab="CV GPP")
lines(testdataBcv$b10km, pred_GPPBcv,col="green", lwd=5)
lines(testdata4cv$b10km, pred_GPP4cv, col="orange", lwd=5)
dev.off()
