#######################################################################################################################
##Continental GAM plots (figure_3 in manuscript)

+#---------------------------------------------------#
  +#### 1. GAM models ####
+#---------------------------------------------------#
  
##read data
setwd("C:/D_workstation/chap_1/cont_analysis/figures/figure_3")
data = read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/0112_IBRA_KAV.csv", stringsAsFactors=FALSE)
library(mgcv)
library(plyr)
library(Cairo)

R1 <- subset(data, run=="1",
             select=x:b10km)

+#---------------------------------------------------#
  +#### 1. Aus GAM plots smoothscatter ####
+#---------------------------------------------------#

  
##RAIN

fit1 = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log)) 
testdata1 = data.frame(b10km=mean(fit1$model$b10km),
                      temp_10kav=mean(fit1$model$temp_10kav),
                      rain_10kav=seq(min(R1[,7]),max(R1[,7]), length=length(R1$gpp_10kav)))
pred_GPP1 = predict(fit1, newdata=testdata1, type='response')

fitR = gam(gpp_10kav~s(rain_10kav,k=5),data=R1, family=Gamma(link=log))
testdataR = data.frame(rain_10kav=seq(min(R1[,7]),max(R1[,7]), length=length(R1$gpp_10kav)))
pred_GPPR = predict(fitR, newdata=testdataR, type='response')

par(font.axis = 2)
CairoPNG(width = 8090, height = 5000, file = "glob_rain_hold2_cairo.png", canvas="white", bg = "white", units="px", dpi=600)
smoothScatter(R1[,7], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdataR$rain_10kav, pred_GPPR, col="green", lwd=5)
lines(testdata1$rain_10kav, pred_GPP1, col="orange", lwd=5)
dev.off()


##TEMP

fit2 = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log)) 
testdata2 = data.frame(b10km=mean(fit2$model$b10km),
                      rain_10kav=mean(fit2$model$rain_10kav),
                      temp_10kav=seq(min(R1[,8]),max(R1[,8]), length=length(R1$gpp_10kav)))
pred_GPP2 = predict(fit2, newdata=testdata2, type='response')

fitT = gam(gpp_10kav~s(temp_10kav,k=5),data=R1, family=Gamma(link=log))
testdataT = data.frame(temp_10kav=seq(min(R1[,8]),max(R1[,8]), length=length(R1$gpp_10kav)))
pred_GPPT = predict(fitT, newdata=testdataT, type='response')


par(font.axis = 2)
CairoPNG(width = 8090, height = 5000, file = "glob_temp_hold_cairo.png", canvas="white", bg = "white", units="px", dpi=600)
smoothScatter(R1[,8], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdataT$temp_10kav,pred_GPPT,col="green", lwd=5)
lines(testdata2$temp_10kav, pred_GPP2, col="orange", lwd=5)
dev.off()

##ALPHA

fit3 = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(alpha,k=5), data=R1, family=Gamma(link=log))
testdata3 = data.frame(rain_10kav=mean(fit3$model$rain_10kav),
                      temp_10kav=mean(fit3$model$temp_10kav),
                      alpha=seq(min(R1[,12]),max(R1[,12]), length=length(R1$gpp_10kav)))
pred_GPP3 = predict(fit3, newdata=testdata3, type='response')

fitA = gam(gpp_10kav~s(alpha,k=5),data=R1, family=Gamma(link=log))
testdataA = data.frame(alpha=seq(min(R1[,12]),max(R1[,12]), length=length(R1$gpp_10kav)))
pred_GPPA = predict(fitA, newdata=testdataA, type='response')

par(font.axis = 2)
CairoPNG(width = 8090, height = 5000, file = "glob_alpha_hold_cairo.png", canvas="white", bg = "white", units="px", dpi=600)
smoothScatter(R1[,12], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdataA$alpha, pred_GPPA,col="green", lwd=5)
lines(testdata3$alpha, pred_GPP3, col="orange", lwd=5)
dev.off()


##BETA

fit4 = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log))
testdata4 = data.frame(rain_10kav=mean(fit4$model$rain_10kav),
                      temp_10kav=mean(fit4$model$temp_10kav),
                      b10km=seq(min(R1[,13]),max(R1[,13]), length=length(R1$gpp_10kav)))
pred_GPP4 = predict(fit4, newdata=testdata4, type='response')

fitB = gam(gpp_10kav~s(b10km,k=5),data=R1, family=Gamma(link=log))
testdataB = data.frame(b10km=seq(min(R1[,13]),max(R1[,13]), length=length(R1$gpp_10kav)))
pred_GPPB = predict(fitB, newdata=testdataB, type='response')

par(font.axis = 2)
CairoPNG(width = 8090, height = 5000, file = "glob_beta_hold_cairo.png", canvas="white", bg = "white", units="px", dpi=600)
smoothScatter(R1[,13], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdataB$b10km, pred_GPPB,col="green", lwd=5)
lines(testdata4$b10km, pred_GPP4, col="orange", lwd=5)
dev.off()
