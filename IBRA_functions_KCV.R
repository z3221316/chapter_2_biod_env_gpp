model_1 = function(x){
  fit = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(b10km,k=5), data=x, family=Gamma(link=log))
  fit_env = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5), data=x, family=Gamma(link=log))
  sum = summary(fit)
  sum_env = summary(fit_env)
  cof = coef(fit) 
  anova = anova(fit, fit_env, test="Chisq")
  dAIC = AIC(fit) - AIC(fit_env)
  dBIC = BIC(fit) - BIC(fit_env)
  dDEV = sum$dev.expl - sum_env$dev.expl
  testdata = data.frame(rain_10kcv=mean(fit$model$rain_10kcv),
                        temp_10kcv=mean(fit$model$temp_10kcv),
                        b10km=seq(min(x[,13]),max(x[,13]), length=length(x$gpp_10kcv)))
  pred_GPP = predict(fit, newdata=testdata, type='response')
  GPP_max = max(pred_GPP)
  GPP_min = min(pred_GPP)
  dGPP = GPP_max - GPP_min  
  if(which.max(pred_GPP)<which.min(pred_GPP))
    dGPP = dGPP * -1
  data.frame(n=length(x$gpp_10kcv), mod1_beta=sum$s.pv[3], P=anova[2,6], dev.expl=sum$dev.expl, DEV_diff = dDEV,
             AIC_diff=dAIC, BIC_diff=dBIC,              
             GPP_diff = dGPP,
             coef_b1 = cof[10], coef_b2 = cof[11], coef_b3 = cof[12], coef_b4 = cof[13], 
             coef_r1 = cof[2], coef_r2 = cof[3], coef_r3 = cof[4], coef_r4 = cof[5],
             coef_t1 = cof[6], coef_t2 = cof[7], coef_t3 = cof[8], coef_t4 = cof[9], dev=anova[2,4])
}

model_2 = function(x){
  fit = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(alpha,k=5), data=x, family=Gamma(link=log))
  fit_env = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5), data=x, family=Gamma(link=log))
  sum = summary(fit)
  sum_env = summary(fit_env)
  cof = coef(fit) 
  anova = anova(fit, fit_env, test="Chisq")
  dAIC = AIC(fit) - AIC(fit_env)
  dBIC = BIC(fit) - BIC(fit_env)
  dDEV = sum$dev.expl - sum_env$dev.expl
  testdata = data.frame(rain_10kcv=mean(fit$model$rain_10kcv),
                        temp_10kcv=mean(fit$model$temp_10kcv),
                        alpha=seq(min(x[,12]),max(x[,12]), length=length(x$gpp_10kcv)))
  pred_GPP = predict(fit, newdata=testdata, type='response')
  GPP_max = max(pred_GPP)
  GPP_min = min(pred_GPP)
  dGPP = GPP_max - GPP_min  
  if(which.max(pred_GPP)<which.min(pred_GPP))
    dGPP = dGPP * -1
  data.frame(n=length(x$gpp_10kcv), mod2_alpha=sum$s.pv[3], P=anova[2,6], dev.expl=sum$dev.expl, DEV_diff = dDEV,
             AIC_diff=dAIC, BIC_diff=dBIC,              
             GPP_diff = dGPP,
             coef_a1 = cof[10], coef_a2 = cof[11], coef_a3 = cof[12], coef_a4 = cof[13], 
             coef_r1 = cof[2], coef_r2 = cof[3], coef_r3 = cof[4], coef_r4 = cof[5],
             coef_t1 = cof[6], coef_t2 = cof[7], coef_t3 = cof[8], coef_t4 = cof[9], dev=anova[2,4])
}

model_3 = function(x){
  fit = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(alpha,k=5)+s(b10km,k=5),data=x, family=Gamma(link=log))
  fit_alpha = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(alpha,k=5),data=x, family=Gamma(link=log))
  sum = summary(fit)
  sum_alpha = summary(fit_alpha)
  cof = coef(fit) 
  anova = anova(fit, fit_alpha, test="Chisq")
  dAIC = AIC(fit) - AIC(fit_alpha)
  dBIC = BIC(fit) - BIC(fit_alpha)
  dDEV = sum$dev.expl - sum_alpha$dev.expl
  testdata = data.frame(rain_10kcv=mean(fit$model$rain_10kcv),
                        temp_10kcv=mean(fit$model$temp_10kcv),
                        alpha=mean(fit$model$alpha),
                        b10km=seq(min(x[,13]),max(x[,13]), length=length(x$gpp_10kcv)))
  pred_GPP = predict(fit, newdata=testdata, type='response')
  GPP_max = max(pred_GPP)
  GPP_min = min(pred_GPP)
  dGPP = GPP_max - GPP_min  
  if(which.max(pred_GPP)<which.min(pred_GPP))
    dGPP = dGPP * -1
  data.frame(n=length(x$gpp_10kcv), mod3_beta=sum$s.pv[4], P=anova[2,6], dev.expl=sum$dev.expl, DEV_diff = dDEV,
             AIC_diff=dAIC, BIC_diff=dBIC,              
             GPP_diff = dGPP,
             coef_b1 = cof[14], coef_b2 = cof[15], coef_b3 = cof[16], coef_b4 = cof[17], 
             coef_r1 = cof[2], coef_r2 = cof[3], coef_r3 = cof[4], coef_r4 = cof[5],
             coef_t1 = cof[6], coef_t2 = cof[7], coef_t3 = cof[8], coef_t4 = cof[9], dev=anova[2,4])
}

model_4 = function(x){
  fit = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(b10km,k=5), data=x, family=Gamma(link=log))
  fit_env = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5), data=x, family=Gamma(link=log))
  sum = summary(fit)
  sum_env = summary(fit_env)
  cof = coef(fit_env) 
  anova = anova(fit, fit_env, test="Chisq")
  dAIC = AIC(fit_env) - AIC(fit)
  dBIC = BIC(fit_env) - BIC(fit)
  dDEV = sum_env$dev.expl - sum$dev.expl
  testdata = data.frame(b10km=mean(fit$model$b10km),
                        temp_10kcv=mean(fit$model$temp_10kcv),
                        rain_10kcv=seq(min(x[,7]),max(x[,7]), length=length(x$gpp_10kcv)))
  pred_GPP = predict(fit, newdata=testdata, type='response') 
  GPP_max = max(pred_GPP)
  GPP_min = min(pred_GPP)
  dGPP = GPP_max - GPP_min  
  if(which.max(pred_GPP)<which.min(pred_GPP))
    dGPP = dGPP * -1
  data.frame(n=length(x$gpp_10kcv), mod4_rain=sum_env$s.pv[1], P=anova[2,6], dev.expl=sum_env$dev.expl, DEV_diff = dDEV,
             AIC_diff=dAIC, BIC_diff=dBIC,              
             GPP_diff = dGPP,
             coef_r1 = cof[2], coef_r2 = cof[3], coef_r3 = cof[4], coef_r4 = cof[5],
             coef_t1 = cof[6], coef_t2 = cof[7], coef_t3 = cof[8], coef_t4 = cof[9], dev=anova[2,4])
}

model_5 = function(x){
  fit = gam(gpp_10kcv~s(alpha,k=5),data=x, family=Gamma(link=log))
  sum = summary(fit)
  cof = coef(fit)
  testdata = data.frame(alpha=seq(min(x[,12]),max(x[,12]), length=length(x$gpp_10kcv)))
  pred_GPP = predict(fit, newdata=testdata, type='response') 
  GPP_max = max(pred_GPP)
  GPP_min = min(pred_GPP)
  dGPP = GPP_max - GPP_min  
  if(which.max(pred_GPP)<which.min(pred_GPP))
    dGPP = dGPP * -1
  data.frame(n=length(x$gpp_10kcv), mod5_alpha=sum$s.pv[1], dev_expl=sum$dev.expl,
             GPP_diff = dGPP,
             coef_a1 = cof[2], coef_a2 = cof[3], coef_a3 = cof[4], coef_a4 = cof[5])
}

model_6 = function(x){
  fit = gam(gpp_10kcv~s(b10km,k=5),data=x, family=Gamma(link=log))
  sum = summary(fit)
  cof = coef(fit)
  testdata = data.frame(b10km=seq(min(x[,13]),max(x[,13]), length=length(x$gpp_10kcv)))
  pred_GPP = predict(fit, newdata=testdata, type='response') 
  GPP_max = max(pred_GPP)
  GPP_min = min(pred_GPP)
  dGPP = GPP_max - GPP_min  
  if(which.max(pred_GPP)<which.min(pred_GPP))
    dGPP = dGPP * -1
  data.frame(n=length(x$gpp_10kcv), mod6_beta=sum$s.pv[1], dev_expl=sum$dev.expl,
             GPP_diff = dGPP,
             coef_b1 = cof[2], coef_b2 = cof[3], coef_b3 = cof[4], coef_b4 = cof[5])
}