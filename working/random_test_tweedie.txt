###########################################################################################################################
## Mixed GAMs using IBRAs as random factors
setwd="C:/D_workstation/chap_1/cont_analysis/output/IBRA/overall/replicates/10km/kernel/magnitude/mixed"
data<- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/0112_IBRA_KAV.csv", stringsAsFactors=FALSE)
d1 <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/rand_1.csv", stringsAsFactors=FALSE)
d2 <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/rand_2.csv", stringsAsFactors=FALSE)
d3 <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/rand_3.csv", stringsAsFactors=FALSE)
d4 <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/rand_4.csv", stringsAsFactors=FALSE)
d5 <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/rand_5.csv", stringsAsFactors=FALSE)
d6 <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/rand_6.csv", stringsAsFactors=FALSE)
d7 <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/rand_7.csv", stringsAsFactors=FALSE)
d8 <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/rand_8.csv", stringsAsFactors=FALSE)
d9 <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/rand_9.csv", stringsAsFactors=FALSE)
d10 <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/rand_10.csv", stringsAsFactors=FALSE)
library (mgcv)

#####################################################################################
## fit GAMMs
fit_R1 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5),data=d1,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))
fit_env1 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5),data=d1,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))

fit_R2 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5),data=d2,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))
fit_env2 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5),data=d2,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))

fit_R3 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5),data=d3,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))
fit_env3 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5),data=d3,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))

fit_R4 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5),data=d4,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))
fit_env4 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5),data=d4,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))

fit_R5 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5),data=d5,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))
fit_env5 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5),data=d5,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))

fit_R6 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5),data=d6,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))
fit_env6 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5),data=d6,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))

fit_R7 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5),data=d7,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))
fit_env7 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5),data=d7,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))

fit_R8 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5),data=d8,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))
fit_env8 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5),data=d8,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))

fit_R9 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5),data=d9,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))
fit_env9 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5),data=d9,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))

fit_R10 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5),data=d10,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))
fit_env10 = gamm(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5),data=d10,random=list(IBRA=~1), family=Tweedie(1.25,power(.1)))

## compare GAMMs
anova = anova(fit,fit_env,test="F")
data.frame(n=length(x$gpp_10kav), AIC=AIC(fit), BIC=BIC(fit), F=summary(anova)[1,6])

####################################################################################

####################################################################################
## To save results to table
write.csv (sort_pairs, file = paste0(out_dir, "/sort_pairs_AV_600.csv"))
write.csv (results, file = paste0(out_dir, "/results_AV_600.csv"))

## To save plot of linear regression as PNG with set dimensions
png(file = paste0(out_dir, "/sort_pairs_AV_600.png"), width=800,height=629)
plot(sort_pairs[,17], sort_pairs[,18], pch=19,cex=1.5, cex.axis=2) 
abline(reg, col="red", cex=2, lwd=3)
dev.off()

## To save diagnostic plots as PNG with set dimensions
png(file = paste0(out_dir, "/pairs_AV_600_resid.png"), width=800,height=629)
plot(reg, which=1, pch=19,cex=1.5, cex.axis=2, lwd=3)
dev.off()

png(file = paste0(out_dir, "/pairs_AV_600_quantile.png"), width=800,height=629)
plot(reg, which=2, pch=19,cex=1.5, cex.axis=2, lwd=3)
dev.off()
