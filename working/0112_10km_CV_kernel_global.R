#Runs gams on average gpp (y) with CV environmental conditions (X) for 2001-12 using 10 sets of random points (n~8670) 
setwd("C:/D_workstation/chap_1/cont_analysis/output/IBRA/overall/replicates/spatially_global/variation")
data <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/0112_IBRA_kernel.csv", stringsAsFactors=FALSE)
library(mgcv)
library(plyr) 

#specify GAM functions
model_1 = function(x){
  fit = gam(gpp_10kcv~s(rain_10kcv,k=3)+s(temp_10kcv,k=3)+s(sol_10kcv,k=3)+s(b10km,k=3),data=x)
  fit_env = gam(gpp_10kcv~s(rain_10kcv,k=3)+s(temp_10kcv,k=3)+s(sol_10kcv,k=3),data=x)
  anova = anova(fit,fit_env,test="F")
  png(file="mod1_resid.png",width=800,height=629)
  plot(residuals(fit)~fitted(fit),
       xlab="Fitted values",ylab="Residuals", pch=19,cex=1.5, cex.axis=2)
  abline(h=0,col="red", cex=2, lwd=3)
  dev.off()
  data.frame(n=length(x$gpp_10kcv), mod1_int=summary(fit)$p.pv[1], beta=summary(fit)$s.pv[4], dev.expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit), F=summary(anova)[1,6])
}

model_2 = function(x){
  fit = gam(gpp_10kcv~s(rain_10kcv,k=3)+s(temp_10kcv,k=3)+s(sol_10kcv,k=3)+s(alpha,k=3),data=x)
  fit_env = gam(gpp_10kcv~s(rain_10kcv,k=3)+s(temp_10kcv,k=3)+s(sol_10kcv,k=3),data=x)
  anova = anova(fit,fit_env,test="F")
  data.frame(n=length(x$gpp_10kcv), mod2_int=summary(fit)$p.pv[1], alpha=summary(fit)$s.pv[4], dev.expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit), F=summary(anova)[1,6])
}

model_3 = function(x){
  fit = gam(gpp_10kcv~s(rain_10kcv,k=3)+s(temp_10kcv,k=3)+s(sol_10kcv,k=3)+s(alpha,k=3)+s(b10km,k=3),data=x)
  fit_alpha =gam(gpp_10kcv~s(rain_10kcv,k=3)+s(temp_10kcv,k=3)+s(sol_10kcv,k=3)+s(alpha,k=3),data=x)
  anova = anova(fit,fit_alpha,test="F")
  data.frame(n=length(x$gpp_10kcv), mod3_int=summary(fit)$p.pv[1], beta=summary(fit)$s.pv[4], dev.expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit), F=summary(anova)[1,6])
}

model_4 = function(x){
  fit = gam(gpp_10kcv~s(rain_10kcv,k=3)+s(temp_10kcv,k=3)+s(sol_10kcv,k=3),data=x)
  data.frame(n=length(x$gpp_10kcv), mod4_int=summary(fit)$p.pv[1], dev_expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit))
}

model_5 = function(x){
  fit = gam(gpp_10kcv~s(alpha,k=3),data=x)
  data.frame(n=length(x$gpp_10kcv), mod5_int=summary(fit)$p.pv[1], alpha=summary(fit)$s.pv[1], dev_expl=summary(fit)$dev.expl)
}

model_6 = function(x){
  fit = gam(gpp_10kcv~s(b10km,k=3),data=x)
  data.frame(n=length(x$gpp_10kcv), mod6_int=summary(fit)$p.pv[1], beta=summary(fit)$s.pv[1], dev_expl=summary(fit)$dev.expl)
}

#split/apply/combine using selections as runlicates
fitted.gam.model.1 = ddply(data, .(run), model_1)
fitted.gam.model.2 = ddply(data, .(run), model_2)
fitted.gam.model.3 = ddply(data, .(run), model_3)
fitted.gam.model.4 = ddply(data, .(run), model_4)
fitted.gam.model.5 = ddply(data, .(run), model_5)
fitted.gam.model.6 = ddply(data, .(run), model_6)

#save results to table
write.csv(fitted.gam.model.1,"mod_1_av_0112.csv")
write.csv(fitted.gam.model.2,"mod_2_av_0112.csv")
write.csv(fitted.gam.model.3,"mod_3_av_0112.csv")
write.csv(fitted.gam.model.4,"mod_4_av_0112.csv")
write.csv(fitted.gam.model.5,"mod_5_av_0112.csv")
write.csv(fitted.gam.model.6,"mod_6_av_0112.csv")

##create residuals
envfit = gam(gpp_av~s(rain_av,k=3)+s(temp_av,k=3)+s(sol_av,k=3),data=data)
resid.fit=residuals(fit_env)
write.csv(resid.fit,"0112_10km_AV_kernel_env_resid.csv")