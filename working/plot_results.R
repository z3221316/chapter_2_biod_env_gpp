#######################################################################################################################
##Result plots

+#---------------------------------------------------#
  +#### 1. GAM models ####
+#---------------------------------------------------#

##read data
setwd("C:/D_workstation/chap_1/cont_analysis/figures/figure_2")
data = read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/0112_IBRA_KAV.csv", stringsAsFactors=FALSE)
library(mgcv)
library(plyr)

R1 <- subset(data, run=="1",
             select=x:b10km)

##run models
model.1 = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log))
model.2 = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(alpha,k=5), data=data, family=Gamma(link=log))
model.3 = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(alpha,k=5)+s(b10km,k=5), data=data, family=Gamma(link=log))
model.4 = gam(gpp_10kcv ~ s(rain_10kav,k=5) + s(temp_10kav,k=5), data=data)
model.6 = gam(gpp_10kav~s(b10km,k=5),data=data, family=Gamma(link=log))
model.5 = gam(gpp_10kav~s(alpha,k=5),data=data, family=Gamma(link=log))
out = summary(model.x)
out

+#---------------------------------------------------#
  +#### 1. plot pair correlations ####
+#---------------------------------------------------#

##read just the variables
corav = read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/0112_IBRA_KAV_cor.csv", stringsAsFactors=FALSE)
setwd("C:/D_workstation/chap_1/cont_analysis/figures/figure_2")

corav_R1 <- subset(corav, run=="1",
             select=gpp_10kav:b10km)

png(file="cont_pairs.png",width=800,height=629)
pairs(corav_R1, panel = function(...) smoothScatter(..., nrpoints = 0, add = TRUE), cex.axis=1.2, lower.panel = NULL)
dev.off()

corav_ARC7 <- subset(corav, replicate=="ARC7",
                   select=gpp_10kav:b10km)

png(file="ARC7_pairs.png",width=800,height=629)
pairs(corav_ARC7, panel = function(...) smoothScatter(..., nrpoints = 0, add = TRUE), cex.axis=1.2, upper.panel = NULL)
dev.off()

+#---------------------------------------------------#
  +#### 1. Aus GAM plots smoothscatter ####
+#---------------------------------------------------#
  
##create predict line
testdata = data.frame(rain_10kav=mean(fit$model$rain_10kav),
                      temp_10kav=mean(fit$model$temp_10kav),
                      b10km=seq(min(x[,13]),max(x[,13]), length=length(x$gpp_10kav)))
pred_GPP = predict(fit, newdata=testdata, type='response')
beta=seq(min(x[,13]),max(x[,13]), length=length(x$gpp_10kav))

  
png(file="glob_rain.png",width=800,height=629)
smoothScatter(data[,7], data[,6], nrpoints = 0, cex.axis=2.3, box(lwd=3), xlab="rain", ylab="gpp_10kav")
dev.off()

png(file="global_rain_GAM.png",width=800,height=629)
plot(model.x, select=1, pch=19, cex=2, cex.axis=2.3)
dev.off()

png(file="glob_temp.png",width=800,height=629)
smoothScatter(data[,8], data[,6], nrpoints = 0, cex.axis=2.3, box(lwd=3), xlab="temp", ylab="gpp_10kav")
dev.off()

png(file="global_temp_GAM.png",width=800,height=629)
plot(model.x, select=2, pch=19, cex=2, cex.axis=2.3)
dev.off()

png(file="glob_alpha.png",width=800,height=629)
smoothScatter(data[,12], data[,6], nrpoints = 0, cex.axis=2.3, box(lwd=3), xlab="alpha", ylab="gpp_10kav")
dev.off()

png(file="global_alpha_GAM.png",width=800,height=629)
plot(model.x, select=3, pch=19, cex=2, cex.axis=2.3)
dev.off()

png(file="glob_beta.png",width=800,height=629)
smoothScatter(data[,13], data[,6], nrpoints = 0, cex.axis=2.3, box(lwd=3), xlab="beta", ylab="gpp_10kav")
dev.off()

png(file="global_beta_GAM.png",width=800,height=629)
plot(model.x, select=4, pch=19, cex=2, cex.axis=2.3)
dev.off()

+#---------------------------------------------------#
  +#### 1. IBRA GAM plots ####
+#---------------------------------------------------#

##subset data
setwd("C:/D_workstation/GPP_Beta_analysis/output/assumptions/m1av")
TIW2 <- subset(data, replicate=="TIW2",
                  select=x:b10km)

SCP9 <- subset(data, replicate=="SCP9",
               select=x:b10km)

DAC7 <- subset(data, replicate=="DAC7",
               select=x:b10km)

CMC1 <- subset(data, replicate=="CMC1",
              select=x:b10km)

SWA8 <- subset(data, replicate=="SWA8",
              select=x:b10km)

##plot_GAM
png(file="File_GAM.png",width=800,height=629)
plot(model.x, residuals=T, pch=19, cex=2, cex.axis=2.3, box(lwd=3), ylab="gpp_10kav")
dev.off()

##plot_RAW
png(file="File_RAW.png",width=800,height=629)
plot(data[,beta], data[,gpp], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="b10km", ylab="gpp_10kav")
dev.off()

##plot gams splines with variable y-axis rather than spline
plot(model.x, residuals=T, pch=19, cex=1.5, cex.axis=2, ylab="gpp_10kav")

##create residuals and plot them
resid.fit=residuals(model.x)
write.csv(resid.fit,"NAME_resid.csv")

png(file="file_mod_resid.png",width=800,height=629)
plot(residuals(model.X)~fitted(model.x),
     xlab="Fitted values",ylab="Residuals", pch=19,cex=1.5, cex.axis=2)
abline(h=0,col="red", cex=3, lwd=3)
dev.off()

png(file="file_env_resid.png",width=800,height=629)
plot(data[,beta], data[,env_resid], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="b10km", ylab="env_resid")
abline(h=0,col="red", cex=2)
dev.off()

+#---------------------------------------------------#
  +#### 1. Boxplots of + and - ####
+#---------------------------------------------------#

##read summary data
results = read.csv("C:/D_workstation/GPP_Beta_analysis/output/IBRA_all_GAM_zonal_plots.csv", stringsAsFactors=FALSE)
setwd("C:/D_workstation/chap_1/cont_analysis/figures/figure_3")

##GPP

png(file="bp_GPPav_beta.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_GPPav~m1av_sign, ylab="av GPP", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_GPPcv_beta.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_GPPcv~m1cv_sign, ylab="cv GPP", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_GPPav_alpha.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_GPPav~m2av_sign, ylab="av GPP", xlab="env + alpha", cex.axis=1.8, data=results)
dev.off()

png(file="bp_GPPcv_alpha.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_GPPcv~m2cv_sign, ylab="cv GPP", xlab="env + alpha", cex.axis=1.8,  data=results)
dev.off()

##diversity

png(file="bp_beta_av.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_BETA~m1av_sign, ylab="beta", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_beta_cv.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_BETA~m1cv_sign, ylab="beta", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_alpha_av.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_ALPHA~m2av_sign, ylab="alpha", xlab="env + alpha", cex.axis=1.8, data=results)
dev.off()

png(file="bp_alpha_cv.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_ALPHA~m2cv_sign, ylab="alpha", xlab="env + alpha", cex.axis=1.8,  data=results)
dev.off()

##Rain

png(file="bp_rain_beta_av.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_rn_av~m1av_sign, ylab="average rainfall (mm)", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_rain_beta_cv.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_rn_cv~m1cv_sign, ylab="cv rainfall", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_rain_alpha_av.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_rn_av~m2av_sign, ylab="average rainfall (mm)", xlab="env + alpha", cex.axis=1.8, data=results)
dev.off()

png(file="bp_rain_alpha_cv.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_rn_cv~m2cv_sign, ylab="cv rainfall", xlab="env + alpha", cex.axis=1.8, data=results)
dev.off()

+#---------------------------------------------------#
  +#### 1. Scatter plots of results ####
+#---------------------------------------------------#

##scatter plots of deviance difference for covariates, using final results
results = read.csv("C:/D_workstation/GPP_Beta_analysis/output/IBRA_all_GAM_zonal_plots.csv", stringsAsFactors=FALSE)
setwd("C:/D_workstation/chap_1/cont_analysis/figures/figure_3")

##GPP

png(file="gpp_m1av.png",width=800,height=629)
par(font.axis = 2)
plot(results[,14], results[,21], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="GPP", ylab="m1av_DEV")
dev.off()

png(file="gpp_m1cv.png",width=800,height=629)
par(font.axis = 2)
plot(results[,14], results[,41], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="GPP", ylab="m1cv_DEV")
dev.off()

png(file="gpp_m2av.png",width=800,height=629)
par(font.axis = 2)
plot(results[,14], results[,26], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="GPP", ylab="m2av_DEV")
dev.off()

png(file="gpp_m2cv.png",width=800,height=629)
par(font.axis = 2)
plot(results[,14], results[,46], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="GPP", ylab="m2cv_DEV")
dev.off()

##diversity

png(file="beta_m1av.png",width=800,height=629)
par(font.axis = 2)
plot(results[,10], results[,21], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="b10km", ylab="m1av_DEV")
dev.off()

png(file="beta_m1cv.png",width=800,height=629)
par(font.axis = 2)
plot(results[,10], results[,41], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="b10km", ylab="m1cv_DEV")
dev.off()

png(file="alpha_m2av.png",width=800,height=629)
par(font.axis = 2)
plot(results[,12], results[,26], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="alpha", ylab="m2av_DEV")
dev.off()

png(file="alpha_m2cv.png",width=800,height=629)
par(font.axis = 2)
plot(results[,12], results[,46], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="alpha", ylab="m2cv_DEV")
dev.off()

##rain

png(file="rain_m1av.png",width=800,height=629)
par(font.axis = 2)
plot(results[,16], results[,21], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="rain", ylab="m1av_DEV")
dev.off()

png(file="rain_m1cv.png",width=800,height=629)
par(font.axis = 2)
plot(results[,16], results[,41], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="rain", ylab="m1cv_DEV")
dev.off()

png(file="rain_m2av.png",width=800,height=629)
par(font.axis = 2)
plot(results[,16], results[,26], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="rain", ylab="m2av_DEV")
dev.off()

png(file="rain_m2cv.png",width=800,height=629)
par(font.axis = 2)
plot(results[,16], results[,46], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="rain", ylab="m2cv_DEV")
dev.off()
