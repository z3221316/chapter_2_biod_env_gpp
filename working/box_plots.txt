##GPP

png(file="bp_GPPav_beta.png",width=400,height=500, res = 400)
par(font.axis = 2)
boxplot(MEAN_GPPav~m1av_sign, ylab="av GPP", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_GPPcv_beta.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_GPPcv~m1cv_sign, ylab="cv GPP", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_GPPav_alpha.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_GPPav~m2av_sign, ylab="av GPP", xlab="env + alpha", cex.axis=1.8, data=results)
dev.off()

png(file="bp_GPPcv_alpha.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_GPPcv~m2cv_sign, ylab="cv GPP", xlab="env + alpha", cex.axis=1.8,  data=results)
dev.off()

##diversity

png(file="bp_beta_av.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_BETA~m1av_sign, ylab="av GPP", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_beta_cv.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_BETA~m1cv_sign, ylab="av GPP", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_alpha_av.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_ALPHA~m2av_sign, ylab="cv GPP", xlab="env + alpha", cex.axis=1.8, data=results)
dev.off()

png(file="bp_alpha_cv.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_ALPHA~m2cv_sign, ylab="cv GPP", xlab="env + alpha", cex.axis=1.8,  data=results)
dev.off()

##Rain

png(file="bp_rain_beta_av.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_rn_av~m1av_sign, ylab="average rainfall (mm)", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_rain_beta_cv.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_rn_cv~m1cv_sign, ylab="cv rainfall", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_rain_alpha_av.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_rn_av~m2av_sign, ylab="average rainfall (mm)", xlab="env + alpha", cex.axis=1.8, data=results)
dev.off()

png(file="bp_rain_alpha_cv.png",width=400,height=500)
par(font.axis = 2)
boxplot(MEAN_rn_cv~m2cv_sign, ylab="cv rainfall", xlab="env + alpha", cex.axis=1.8, data=results)
dev.off()

##temp

png(file="bp_5.png",width=400,height=500)
boxplot(MEAN_tp_av~m1av_sign, ylab="average temp (C)", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_6.png",width=400,height=500)
boxplot(MEAN_tp_av~m2av_sign, ylab="average temp (C)", xlab="env + alpha", cex.axis=1.8, data=results)
dev.off()

png(file="bp_7.png",width=400,height=500)
boxplot(MEAN_tp_cv~m1cv_sign, ylab="cv temp", xlab="env + b10km", cex.axis=1.8, data=results)
dev.off()

png(file="bp_8.png",width=400,height=500)
boxplot(MEAN_tp_cv~m2cv_sign, ylab="cv temp", xlab="env + alpha", cex.axis=1.8, data=results)
dev.off()

