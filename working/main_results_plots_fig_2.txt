#######################################################################################################################
##IBRA plots

+#---------------------------------------------------#
  +#### 1. GAM models ####
+#---------------------------------------------------#

##read data
setwd("C:/D_workstation/chap_1/cont_analysis/figures/figure_2/maps")
data = read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/0112_IBRA_KAV.csv", stringsAsFactors=FALSE)
library(mgcv)
library(plyr)

##subset data to IBRAs replicates with strongest relationships

setwd("C:/D_workstation/GPP_Beta_analysis/output/assumptions/m1av")
ARC <- subset(data, replicate=="ARC7",
                  select=x:b10km)
				  
DEU <- subset(data, replicate=="DEU2",
                  select=x:b10km)

RIV <- subset(data, replicate=="RIV10",
               select=x:b10km)

MII <- subset(data, replicate=="MII6",
               select=x:b10km)

##run models
model.ARC = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=ARC, family=Gamma(link=log))
model.DEU = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(b10km,k=5), data=DEU, family=Gamma(link=log))
model.RIV = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(alpha,k=5), data=RIV, family=Gamma(link=log))
model.MII = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(alpha,k=5), data=MII, family=Gamma(link=log))
			  
+#---------------------------------------------------#
          +#### 1. IBRA GAM plots ####
+#---------------------------------------------------#

##plot_GAM

png(file="M1AV_ARC7_GAM.png",width=800,height=629)
par(font.axis = 2)
plot(model.x, residuals=T, pch=19, cex=2, cex.axis=2.3, box(lwd=3), xlab="", ylab="")
dev.off()

png(file="M1CV_DEU2_GAM.png",width=800,height=629)
par(font.axis = 2)
plot(model.x, residuals=T, pch=19, cex=2, cex.axis=2.3, box(lwd=3), xlab="", ylab="")
dev.off()

png(file="M2AV_RIV10_GAM.png",width=800,height=629)
par(font.axis = 2)
plot(model.x, residuals=T, pch=19, cex=2, cex.axis=2.3, box(lwd=3), xlab="", ylab="")
dev.off()

png(file="M2CV_MII6_GAM.png",width=800,height=629)
par(font.axis = 2)
plot(model.x, residuals=T, pch=19, cex=2, cex.axis=2.3, box(lwd=3), xlab="", ylab="")
dev.off()

##plot_RAW

png(file="ARC7_AV_RAW.png",width=800,height=629)
par(font.axis = 2)
plot(ARC[,13], ARC[,6], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="", ylab="")
dev.off()

png(file="DEU2_CV_RAW.png",width=800,height=629)
par(font.axis = 2)
plot(DEU[,13], DEU[,9], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="", ylab="")
dev.off()

png(file="RIV10_RAW.png",width=800,height=629)
par(font.axis = 2)
plot(RIV[,12], RIV[,6], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="", ylab="")
dev.off()

png(file="MII6_CV_RAW.png",width=800,height=629)
par(font.axis = 2)
plot(MII6[,12], MII6[,9], pch = 19, cex=2, cex.axis=2.3, box(lwd=3), xlab="", ylab="")
dev.off()


+#---------------------------------------------------#
  +#### 1. Boxplots of + and - ####
+#---------------------------------------------------#


