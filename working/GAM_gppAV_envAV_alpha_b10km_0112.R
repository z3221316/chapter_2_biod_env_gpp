#Runs gams on average gpp (y) with average environmental conditions (X) for 2001-12 using 10 sets of random points (n~8670) 
setwd("C:/D_workstation/chap_1/cont_analysis/output/IBRA/overall/replicates")
data <- read.csv("C:/D_workstation/chap_1/cont_analysis/data/overall/0112_replicates.csv", stringsAsFactors=FALSE)
library(mgcv)
library(plyr) 

#specify GAM functions
model_1 = function(x){
  fit =gam(gpp_av~s(rain_av,k=3)+s(temp_av,k=3)+s(sol_av,k=3)+s(b10km,k=3),data=x)
  data.frame(n=length(x$gpp_av), int=summary(fit)$p.pv[1], beta=summary(fit)$s.pv[4], dev.expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit))
}

model_2 = function(x){
  fit =gam(gpp_av~s(rain_av,k=3)+s(temp_av,k=3)+s(sol_av,k=3)+s(alpha,k=3),data=x)
  data.frame(n=length(x$gpp_av), int=summary(fit)$p.pv[1], alpha=summary(fit)$s.pv[4], dev.expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit))
}

model_3 = function(x){
  fit =gam(gpp_av~s(rain_av,k=3)+s(temp_av,k=3)+s(sol_av,k=3)+s(alpha,k=3)+s(b10km,k=3),data=x)
  data.frame(n=length(x$gpp_av), int=summary(fit)$p.pv[1], dev.expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit))
}

model_4 = function(x){
  fit = gam(gpp_av~s(rain_av,k=3)+s(temp_av,k=3)+s(sol_av,k=3),data=x)
  data.frame(n=length(x$gpp_av), int=summary(fit)$p.pv[1], dev_expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit))
}

model_5 = function(x){
  fit = gam(gpp_av~s(alpha,k=3),data=x)
  data.frame(n=length(x$gpp_av), int=summary(fit)$p.pv[1], alpha=summary(fit)$s.pv[1], dev_expl=summary(fit)$dev.expl)
}

model_6 = function(x){
  fit = gam(gpp_av~s(b10km,k=3),data=x)
  data.frame(n=length(x$gpp_av), int=summary(fit)$p.pv[1], beta=summary(fit)$s.pv[1], dev_expl=summary(fit)$dev.expl)
}

#split/apply/combine using IBRA regions as replicates
fitted.gam.model.1 = ddply(data, .(replicate), model_1)
fitted.gam.model.2 = ddply(data, .(replicate), model_2)
fitted.gam.model.3 = ddply(data, .(replicate), model_3)
fitted.gam.model.4 = ddply(data, .(replicate), model_4)
fitted.gam.model.5 = ddply(data, .(replicate), model_5)
fitted.gam.model.6 = ddply(data, .(replicate), model_6)

#save results to table
write.csv(fitted.gam.model.1,"gppAV_envAV_b10km_0112.csv")
write.csv(fitted.gam.model.2,"gppAV_envAV_alpha_0112.csv")
write.csv(fitted.gam.model.3,"gppAV_envAV_alpha_b10km_0112.csv")
write.csv(fitted.gam.model.4,"gppAV_envAV_0112.csv")
write.csv(fitted.gam.model.5,"gppAV_alpha_0112.csv")
write.csv(fitted.gam.model.6,"gppAV_b10km_0112.csv")