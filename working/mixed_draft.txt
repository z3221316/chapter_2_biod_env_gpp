###########################################################################################################################
## Mixed GAMs  
data <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/0112_IBRA_kernel_weights_random_1.csv", stringsAsFactors=FALSE)
library (mgcv)
library (nlme)

corAR1
corSpher

fit = gamm(gpp_10kav~s(rain_10kav,k=3)+s(temp_10kav,k=3)+s(b10km,k=3),data=data,random=list(IBRA=~1,SampNo=~1), family=Gamma(link=log))

fitR =  gamm(gpp_10kav~s(rain_10kav,k=3)+s(temp_10kav,k=3)+s(b10km,k=3), correlation = corAR1(form =~ x + y), data=data, random=list(IBRA_no=~1,SampNo=~1), family=Gamma(link=log))

fitR2 = gamm(gpp_10kav~s(rain_10kav,k=3)+s(temp_10kav,k=3)+s(b10km,k=3), data=data, random=list(IBRA_no=~1,SampNo=~1), family=Gamma(link=log))



fit = gam(gpp_10kav ~ s(rain_10kav,k=3) + s(x,y,by=rain_10kav) + s(temp_10kav,k=3) + s(b10km,k=3), data=data, family=Gamma(link=log))   0.8185773

fit2 = gam(gpp_10kav ~ s(rain_10kav,k=3) + s(temp_10kav,k=3) + s(b10km,k=3), data=data, family=Gamma(link=log))                         0.7339867

fit3 = gam(gpp_10kav ~ s(rain_10kav,k=3) + s(x,y,by=rain_10kav) + s(temp_10kav,k=3), data=data, family=Gamma(link=log))                 0.8180709

fit4 = gam(gpp_10kav ~ s(b10km,k=3) + s(x,y,by=b10km), data=data, family=Gamma(link=log))                                               0.7834718

fit5 = gam(gpp_10kav ~ s(b10km,k=3), data=data, family=Gamma(link=log))                                                                 0.1185863

Hi everyone,

My name is Hugh and I'm using the mgcv package in R to run GAMs. Not my real data but an EG:

a = rnorm(100)
b = runif(100)
y = a*b/(a+b)

mod = gam(y~s(a, k = 3)+s(b, k = 3))

I'm after an indication of whether the overall effect of the variable "b" on "y" is positive 
or negative. This is for a hypothesis test of the GAM with and without "b", giving an 
indication of what the direction is rather than just significance, AIC, etc. 

Originally I was thinking about using "coef(mod)" to get the coefficients for "b", but not sure 
if that is appropriate, EG with 3 knots:

     s(b).1        
  1.733370e-12 
  
     s(b).2
  -1.257469e+00

  (Intercept)  
-1.162097e+00

Another option could be using "predict" to get the predicted values for "y", "a" & "b" using 
original data, and then compare the max and min, or perhaps compare the intercept for "b" 
with the final predicted value? I'm new to R so any advice would be greatly appreciated,

Hugh

This seems a bit 
like the slope associated with the "b" smooth term, but not straightforward because the 
smoother (for my real data) often have a curvilinear/hump shape?


fit =  gam (y ~ s(a, k = 3) + s(b, k = 3) + s(c, k = 3), data =  data)

fit =  gam (y ~ s(a, k = 3) + s(b, k = 3) + s(c, k = 3), data =  data)





s(c).1      s(c).2 
0.05988576     -0.01706053 

  (Intercept)       s(b).1        s(b).2 
-1.162097e+00  1.733370e-12 -1.257469e+00 

Another option could be using "predict" to get the predicted values for "Y", "a" & "b" using original data, and then 
compare the max and min, or perhaps comparing the intercept for "b" with the last (i.e. end of x-axis) predicted value? 
I'm very new to R so any advice would be greatly appreciated,

Hugh

pred_response = predict(fit, data, type="response"), se.fit = TRUE)
write.csv(pred,"pred_resp.csv")
pred_terms = predict(fit, data, type = "terms", se.fit = TRUE)
write.csv(pred_terms,"pred_terms.csv")

