#Runs gams on point coefficient of variaton gpp (y) with cv of environmental conditions (X) for 2001-12 in each IBRA using 10 sets of random points (n~120/8670) 
setwd("C:/D_workstation/chap_1/cont_analysis/output/IBRA/overall/replicates/10km/variation")
data <- read.csv("C:/D_workstation/chap_1/cont_analysis/data/overall/0112_IBRA_points.csv", stringsAsFactors=FALSE)
library(mgcv)
library(plyr) 

#specify GAM functions
model_1 = function(x){
  fit =gam(gpp_cv~s(rain_cv,k=3)+s(temp_cv,k=3)+s(sol_cv,k=3)+s(b10km,k=3),data=x)
  data.frame(n=length(x$gpp_cv), mod1_int=summary(fit)$p.pv[1], beta=summary(fit)$s.pv[4], dev.expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit))
}

model_2 = function(x){
  fit =gam(gpp_cv~s(rain_cv,k=3)+s(temp_cv,k=3)+s(sol_cv,k=3)+s(alpha,k=3),data=x)
  data.frame(n=length(x$gpp_cv), mod2_int=summary(fit)$p.pv[1], alpha=summary(fit)$s.pv[4], dev.expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit))
}

model_3 = function(x){
  fit =gam(gpp_cv~s(rain_cv,k=3)+s(temp_cv,k=3)+s(sol_cv,k=3)+s(alpha,k=3)+s(b10km,k=3),data=x)
  data.frame(n=length(x$gpp_cv), mod3_int=summary(fit)$p.pv[1], dev.expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit))
}

model_4 = function(x){
  fit = gam(gpp_cv~s(rain_cv,k=3)+s(temp_cv,k=3)+s(sol_cv,k=3),data=x)
  data.frame(n=length(x$gpp_cv), mod4_int=summary(fit)$p.pv[1], dev_expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit))
}

model_5 = function(x){
  fit = gam(gpp_cv~s(alpha,k=3),data=x)
  data.frame(n=length(x$gpp_cv), mod5_int=summary(fit)$p.pv[1], alpha=summary(fit)$s.pv[1], dev_expl=summary(fit)$dev.expl)
}

model_6 = function(x){
  fit = gam(gpp_cv~s(b10km,k=3),data=x)
  data.frame(n=length(x$gpp_cv), mod6_int=summary(fit)$p.pv[1], beta=summary(fit)$s.pv[1], dev_expl=summary(fit)$dev.expl)
}

#split/apply/combine using IBRA regions as replicates
fitted.gam.model.1 = ddply(data, .(replicate), model_1)
fitted.gam.model.2 = ddply(data, .(replicate), model_2)
fitted.gam.model.3 = ddply(data, .(replicate), model_3)
fitted.gam.model.4 = ddply(data, .(replicate), model_4)
fitted.gam.model.5 = ddply(data, .(replicate), model_5)
fitted.gam.model.6 = ddply(data, .(replicate), model_6)

#save results to table
write.csv(fitted.gam.model.1,"mod_1_cv_0112.csv")
write.csv(fitted.gam.model.2,"mod_2_cv_0112.csv")
write.csv(fitted.gam.model.3,"mod_3_cv_0112.csv")
write.csv(fitted.gam.model.4,"mod_4_cv_0112.csv")
write.csv(fitted.gam.model.5,"mod_5_cv_0112.csv")
write.csv(fitted.gam.model.6,"mod_6_cv_0112.csv")