#######################################################################################################################
##Result plots 
##Fit models to IBRAs
data = read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/0112_IBRA_KAV.csv", stringsAsFactors=FALSE)
library(mgcv)
library(plyr)

##subset data
setwd("C:/D_workstation/GPP_Beta_analysis/output/assumptions/m1cv")
MGD1 <- subset(data, replicate=="MGD1",
               select=x:b10km)

BBN2 <- subset(data, replicate=="BBN2",
               select=x:b10km)

DEU2 <- subset(data, replicate=="DEU2",
               select=x:b10km)

MAL1 <- subset(data, replicate=="MAL1",
               select=x:b10km)

DRP5 <- subset(data, replicate=="DRP5",
               select=x:b10km)

model.1.MGD = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(b10km,k=5), data=MGD1, family=Gamma(link=log))
model.1.BBN = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(b10km,k=5), data=BBN2, family=Gamma(link=log))
model.1.DEU = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(b10km,k=5), data=DEU2, family=Gamma(link=log))
model.1.MAL = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(b10km,k=5), data=MAL1, family=Gamma(link=log))
model.1.DRP = gam(gpp_10kcv~s(rain_10kcv,k=5)+s(temp_10kcv,k=5)+s(b10km,k=5), data=DRP5, family=Gamma(link=log))

##GAMs
png(file="MGD1_GAM.png",width=800,height=629)
plot(model.1.MGD, select=3, residuals=T, pch=19, cex=2, cex.axis=2.3, ylab="gpp_10kcv")
dev.off()

png(file="BBN2_GAM.png",width=800,height=629)
plot(model.1.BBN, select=3, residuals=T, pch=19, cex=2, cex.axis=2.3, ylab="gpp_10kcv")
dev.off()

png(file="DEU2_GAM.png",width=800,height=629)
plot(model.1.DEU, select=3, residuals=T, pch=19, cex=2, cex.axis=2.3, ylab="gpp_10kcv")
dev.off()

png(file="MAL1_GAM.png",width=800,height=629)
plot(model.1.MAL, select=3, residuals=T, pch=19, cex=2, cex.axis=2.3, ylab="gpp_10kcv")
dev.off()

png(file="DRP5_GAM.png",width=800,height=629)
plot(model.1.DRP, select=3, residuals=T, pch=19, cex=2, cex.axis=2.3, ylab="gpp_10kcv")
dev.off()

##RAW
png(file="MGD1_RAW.png",width=800,height=629)
plot(MGD1[,13], MGD1[,9], pch = 19, cex=2, cex.axis=2.3, xlab="b10km", ylab="gpp_10kcv")
dev.off()

png(file="BBN2_RAW.png",width=800,height=629)
plot(BBN2[,13], BBN2[,9], pch = 19, cex=2, cex.axis=2.3, xlab="b10km", ylab="gpp_10kcv")
dev.off()

png(file="DEU2_RAW.png",width=800,height=629)
plot(DEU2[,13], DEU2[,9], pch = 19, cex=2, cex.axis=2.3, xlab="b10km", ylab="gpp_10kcv")
dev.off()

png(file="MAL1_RAW.png",width=800,height=629)
plot(MAL1[,13], MAL1[,9], pch = 19, cex=2, cex.axis=2.3, xlab="b10km", ylab="gpp_10kcv")
dev.off()

png(file="DRP5_RAW.png",width=800,height=629)
plot(DRP5[,13], DRP5[,9], pch = 19, cex=2, cex.axis=2.3, xlab="b10km", ylab="gpp_10kcv")
dev.off()

##RESIDUALS
png(file="MGD1_mod_resid.png",width=800,height=629)
plot(residuals(model.1.MGD)~fitted(model.1.MGD),
     xlab="Fitted values",ylab="Residuals", pch=19,cex=1.5, cex.axis=2)
abline(h=0,col="red", cex=3, lwd=3)
dev.off()

png(file="BBN2_mod_resid.png",width=800,height=629)
plot(residuals(model.1.BBN)~fitted(model.1.BBN),
     xlab="Fitted values",ylab="Residuals", pch=19,cex=1.5, cex.axis=2)
abline(h=0,col="red", cex=3, lwd=3)
dev.off()

png(file="DEU2_mod_resid.png",width=800,height=629)
plot(residuals(model.1.DEU)~fitted(model.1.DEU),
     xlab="Fitted values",ylab="Residuals", pch=19,cex=1.5, cex.axis=2)
abline(h=0,col="red", cex=3, lwd=3)
dev.off()

png(file="MAL1_mod_resid.png",width=800,height=629)
plot(residuals(model.1.MAL)~fitted(model.1.MAL),
     xlab="Fitted values",ylab="Residuals", pch=19,cex=1.5, cex.axis=2)
abline(h=0,col="red", cex=3, lwd=3)
dev.off()

png(file="DRP5_mod_resid.png",width=800,height=629)
plot(residuals(model.1.DRP)~fitted(model.1.DRP),
     xlab="Fitted values",ylab="Residuals", pch=19,cex=1.5, cex.axis=2)
abline(h=0,col="red", cex=3, lwd=3)
dev.off()






