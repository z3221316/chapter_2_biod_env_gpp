###################################################################################################################################################################
##Runs GAMs on neighbourhood gpp (y) with environmental conditions and 10km beta (X) for 2001-12 in each IBRA using 10 sets of random points (n~120/8670)
setwd("C:/D_workstation/chap_1/cont_analysis/output/IBRA/overall/replicates/10km/kernel/magnitude")
data <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/0112_IBRA_KAV.csv", stringsAsFactors=FALSE)
library(mgcv)
library(plyr)
library(arm)
library(reshape)
library(coda)
library(coefplot2)

##function for GAM formula and results
model_1 = function(x){
  fit = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=x, family=Gamma(link=log))
  fit_env = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5), data=x, family=Gamma(link=log))
  sum = summary(fit)
  cof = coef(fit) 
  anova = anova(fit, fit_env, test = "F")
  dAIC = AIC(fit) - AIC(fit_env)
  dBIC = BIC(fit) - BIC(fit_env)
  pred_GPP = predict(fit, x, type = "response")
  b_effect = cbind(x, pred_GPP)
  a = which.max(b_effect$b10km)
  b = which.min(b_effect$b10km)
  data.frame(n=length(x$gpp_10kav), mod1_beta=sum$s.pv[3], dev.expl=sum$dev.expl, 
             coef_b1 = cof[10], coef_b2= cof[11], coef_b3= cof[12], coef_b4= cof[13], 
             coef_r1 = cof[2], coef_r2= cof[3], coef_r3= cof[4], coef_r4= cof[5],
             coef_s1 = cof[6], coef_s2= cof[7], coef_s3= cof[8], coef_s4= cof[9],
             F=summary(anova)[1,6], AIC_diff=dAIC, BIC_diff=dBIC, GPP_max=b_effect[a, 11], GPP_min=b_effect[b, 11])
}

model_2 = function(x){
  fit = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(sol_10kav,k=5)+s(alpha,k=5),data=x)
  fit_env = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(sol_10kav,k=5),data=x)
  anova = anova(fit,fit_env,test="F")
  data.frame(n=length(x$gpp_10kav), mod2_int=summary(fit)$p.pv[1], alpha=summary(fit)$s.pv[4], dev.expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit), F=summary(anova)[1,6], coef_b1= cof[8], coef_b1= cof[8])
}

model_3 = function(x){
  fit = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(alpha,k=5)+s(b10km,k=5),data=x)
  fit_alpha =gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(alpha,k=5),data=x)
  anova = anova(fit,fit_alpha,test="F")
  data.frame(n=length(x$gpp_10kav), mod3_int=summary(fit)$p.pv[1], beta=summary(fit)$s.pv[4], dev.expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit), F=summary(anova)[1,6], coef_b1= cof[8], coef_b1= cof[8])
}

model_4 = function(x){
  fit = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(sol_10kav,k=5),data=x)
  data.frame(n=length(x$gpp_10kav), mod4_int=summary(fit)$p.pv[1], dev_expl=summary(fit)$dev.expl, AIC=AIC(fit), BIC=BIC(fit))
}

model_5 = function(x){
  fit = gam(gpp_10kav~s(alpha,k=5),data=x)
  data.frame(n=length(x$gpp_10kav), mod5_int=summary(fit)$p.pv[1], alpha=summary(fit)$s.pv[1], dev_expl=summary(fit)$dev.expl)
}

model_6 = function(x){
  fit = gam(gpp_10kav~s(b10km,k=5),data=x)
  data.frame(n=length(x$gpp_10kav), mod6_int=summary(fit)$p.pv[1], beta=summary(fit)$s.pv[1], dev_expl=summary(fit)$dev.expl)
}

#split/apply/combine using IBRA regions as replicates
fitted.gam.model.1 = ddply(data, .(replicate), model_1)
fitted.gam.model.2 = ddply(data, .(replicate), model_2)
fitted.gam.model.3 = ddply(data, .(replicate), model_3)
fitted.gam.model.4 = ddply(data, .(replicate), model_4)
fitted.gam.model.5 = ddply(data, .(replicate), model_5)
fitted.gam.model.6 = ddply(data, .(replicate), model_6)

#save results to table
write.csv(fitted.gam.model.1,"mod_1_10kav_0112.csv")
write.csv(fitted.gam.model.2,"mod_2_10kav_0112.csv")
write.csv(fitted.gam.model.3,"mod_3_10kav_0112.csv")
write.csv(fitted.gam.model.4,"mod_4_10kav_0112.csv")
write.csv(fitted.gam.model.5,"mod_5_10kav_0112.csv")
write.csv(fitted.gam.model.6,"mod_6_10kav_0112.csv")