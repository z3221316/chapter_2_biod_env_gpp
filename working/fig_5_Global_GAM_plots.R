#######################################################################################################################
##Result plots

+#---------------------------------------------------#
  +#### 1. GAM models ####
+#---------------------------------------------------#
  
##read data
setwd("C:/D_workstation/chap_1/cont_analysis/figures/figure_5")
data = read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/0112_IBRA_KAV.csv", stringsAsFactors=FALSE)
library(mgcv)
library(plyr)
library(Cairo)

R1 <- subset(data, run=="1",
             select=x:b10km)

+#---------------------------------------------------#
  +#### 1. Aus GAM plots smoothscatter ####
+#---------------------------------------------------#
  
##RAIN
fit = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log)) 
testdata = data.frame(b10km=mean(fit$model$b10km),
                      temp_10kav=mean(fit$model$temp_10kav),
                      rain_10kav=seq(min(R1[,7]),max(R1[,7]), length=length(R1$gpp_10kav)))
pred_GPP = predict(fit, newdata=testdata, type='response')

par(font.axis = 2)
png(file="glob_rain_hold.png",width=800,height=629)
smoothScatter(R1[,7], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdata$rain_10kav, pred_GPP, col="red", lwd=3)
dev.off()

fit = gam(gpp_10kav~s(rain_10kav,k=5),data=R1, family=Gamma(link=log))
testdata = data.frame(rain_10kav=seq(min(R1[,7]),max(R1[,7]), length=length(R1$gpp_10kav)))
pred_GPP = predict(fit, newdata=testdata, type='response')

par(font.axis = 2)
png(file="glob_rain.png",width=800,height=629)
smoothScatter(R1[,7], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdata$rain_10kav,pred_GPP,col="red", lwd=3)
dev.off()

##temp
fit = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log)) 
testdata = data.frame(b10km=mean(fit$model$b10km),
                      rain_10kav=mean(fit$model$rain_10kav),
                      temp_10kav=seq(min(R1[,8]),max(R1[,8]), length=length(R1$gpp_10kav)))
pred_GPP = predict(fit, newdata=testdata, type='response')

par(font.axis = 2)
png(file="glob_temp_hold_.png",width=800,height=629)
smoothScatter(R1[,8], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdata$temp_10kav, pred_GPP, col="red", lwd=3)
dev.off()

fit = gam(gpp_10kav~s(temp_10kav,k=5),data=R1, family=Gamma(link=log))
testdata = data.frame(temp_10kav=seq(min(R1[,8]),max(R1[,8]), length=length(R1$gpp_10kav)))
pred_GPP = predict(fit, newdata=testdata, type='response')

par(font.axis = 2)
png(file="glob_temp_.png",width=800,height=629)
smoothScatter(R1[,8], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdata$temp_10kav,pred_GPP,col="red", lwd=3)
dev.off()

##Alpha
fit = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(alpha,k=5), data=R1, family=Gamma(link=log))
testdata = data.frame(rain_10kav=mean(fit$model$rain_10kav),
                      temp_10kav=mean(fit$model$temp_10kav),
                      alpha=seq(min(R1[,12]),max(R1[,12]), length=length(R1$gpp_10kav)))
pred_GPP = predict(fit, newdata=testdata, type='response')

par(font.axis = 2)
png(file="glob_alpha_hold.png",width=800,height=629)
smoothScatter(R1[,12], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdata$alpha, pred_GPP, col="red", lwd=3)
dev.off()

fit = gam(gpp_10kav~s(alpha,k=5),data=R1, family=Gamma(link=log))
testdata = data.frame(alpha=seq(min(R1[,12]),max(R1[,12]), length=length(R1$gpp_10kav)))
pred_GPP = predict(fit, newdata=testdata, type='response')

par(font.axis = 2)
png(file="glob_alpha.png",width=800,height=629)
smoothScatter(R1[,12], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdata$alpha,pred_GPP,col="red", lwd=3)
dev.off()

##beta
fit = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log))
testdata = data.frame(rain_10kav=mean(fit$model$rain_10kav),
                      temp_10kav=mean(fit$model$temp_10kav),
                      b10km=seq(min(R1[,13]),max(R1[,13]), length=length(R1$gpp_10kav)))
pred_GPP = predict(fit, newdata=testdata, type='response')

par(font.axis = 2)
png(file="glob_b10km_hold.png",width=800,height=629)
smoothScatter(R1[,13], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdata$b10km, pred_GPP, col="red", lwd=3)
dev.off()

fit = gam(gpp_10kav~s(b10km,k=5),data=R1, family=Gamma(link=log))
testdata = data.frame(b10km=seq(min(R1[,13]),max(R1[,13]), length=length(R1$gpp_10kav)))
pred_GPP = predict(fit, newdata=testdata, type='response')

par(font.axis = 2)
png(file="glob_b10km.png",width=800,height=629)
smoothScatter(R1[,13], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdata$b10km,pred_GPP,col="red", lwd=3)
dev.off()
