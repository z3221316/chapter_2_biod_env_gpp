fit1av = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log)) 
testdata1av = data.frame(b10km=mean(fit1$model$b10km),
                      temp_10kav=mean(fit1$model$temp_10kav),
                      rain_10kav=seq(min(R1[,7]),max(R1[,7]), length=length(R1$gpp_10kav)))
pred_GPP1av = predict(fit1, newdata=testdata1, type='response')

fitRav = gam(gpp_10kav~s(rain_10kav,k=5),data=R1, family=Gamma(link=log))
testdataRav = data.frame(rain_10kav=seq(min(R1[,7]),max(R1[,7]), length=length(R1$gpp_10kav)))
pred_GPPRav = predict(fitR, newdata=testdataR, type='response')

fit2av = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log)) 
testdata2av = data.frame(b10km=mean(fit2$model$b10km),
                      rain_10kav=mean(fit2$model$rain_10kav),
                      temp_10kav=seq(min(R1[,8]),max(R1[,8]), length=length(R1$gpp_10kav)))
pred_GPP2av = predict(fit2, newdata=testdata2, type='response')

fitTav = gam(gpp_10kav~s(temp_10kav,k=5),data=R1, family=Gamma(link=log))
testdataTav = data.frame(temp_10kav=seq(min(R1[,8]),max(R1[,8]), length=length(R1$gpp_10kav)))
pred_GPPTav = predict(fitT, newdata=testdataT, type='response')

fit3av = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(alpha,k=5), data=R1, family=Gamma(link=log))
testdata3av = data.frame(rain_10kav=mean(fit3$model$rain_10kav),
                      temp_10kav=mean(fit3$model$temp_10kav),
                      alpha=seq(min(R1[,12]),max(R1[,12]), length=length(R1$gpp_10kav)))
pred_GPP3av = predict(fit3, newdata=testdata3, type='response')

fitAav = gam(gpp_10kav~s(alpha,k=5),data=R1, family=Gamma(link=log))
testdataAav = data.frame(alpha=seq(min(R1[,12]),max(R1[,12]), length=length(R1$gpp_10kav)))
pred_GPPAav = predict(fitA, newdata=testdataA, type='response')

fit4av = gam(gpp_10kav~s(rain_10kav,k=5)+s(temp_10kav,k=5)+s(b10km,k=5), data=R1, family=Gamma(link=log))
testdata4av = data.frame(rain_10kav=mean(fit4$model$rain_10kav),
                      temp_10kav=mean(fit4$model$temp_10kav),
                      b10km=seq(min(R1[,13]),max(R1[,13]), length=length(R1$gpp_10kav)))
pred_GPP4av = predict(fit4, newdata=testdata4, type='response')

fitBav = gam(gpp_10kav~s(b10km,k=5),data=R1, family=Gamma(link=log))
testdataBav = data.frame(b10km=seq(min(R1[,13]),max(R1[,13]), length=length(R1$gpp_10kav)))
pred_GPPBav = predict(fitB, newdata=testdataB, type='response')



CairoPNG(width = 10000, height = 16180, file = "Fig_3_av.png", canvas="white", bg = "white", units="px", dpi=900)
par(mfrow=c(4,1), mar=c(8,8,6,4), mgp=c(4, 1, 0))
par(font.axis = 2)
smoothScatter(R1[,7], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdataR$rain_10kav, pred_GPPR, col="green", lwd=5)
lines(testdata1$rain_10kav, pred_GPP1, col="orange", lwd=5)
par(font.axis = 2)
smoothScatter(R1[,8], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdataT$temp_10kav,pred_GPPT,col="green", lwd=5)
lines(testdata2$temp_10kav, pred_GPP2, col="orange", lwd=5)
par(font.axis = 2)
smoothScatter(R1[,12], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdataA$alpha, pred_GPPA,col="green", lwd=5)
lines(testdata3$alpha, pred_GPP3, col="orange", lwd=5)
par(font.axis = 2)
smoothScatter(R1[,13], R1[,6], nrpoints = 0, pch=19, cex=2, cex.axis=2.3, xlab="", ylab="")
lines(testdataB$b10km, pred_GPPB,col="green", lwd=5)
lines(testdata4$b10km, pred_GPP4, col="orange", lwd=5)
dev.off()