#######################################################################################################################

#+#---------------------------------------------------#
#  +#### GPP IBRA analyses ####+
#+#---------------------------------------------------#
  
#######################################################################################################################
##Runs GAMs on neighbourhood average gpp (y) for 2001-12 with environmental conditions and 10km beta (X) 
## across all IBRAs using 10 sets of random points (n~8670), stratified by IBRA
setwd("C:/D_workstation/chap_1/cont_analysis/output/IBRA/overall/replicates/spatially_global/magnitude")
data <- read.csv("C:/D_workstation/GPP_Beta_analysis/data/tables_shapefiles/0112_IBRA_KAV.csv", stringsAsFactors=FALSE)
library(mgcv)
library(plyr)

##source functions for global AV analyses
source("IBRA_functions_KAV_glob.R")

##split/apply/combine using IBRA regions as runs
fitted.gam.model.1 = ddply(data, .(run), model_1)
fitted.gam.model.2 = ddply(data, .(run), model_2)
fitted.gam.model.3 = ddply(data, .(run), model_3)
fitted.gam.model.4 = ddply(data, .(run), model_4)
fitted.gam.model.5 = ddply(data, .(run), model_5)
fitted.gam.model.6 = ddply(data, .(run), model_6)
fitted.gam.model.7 = ddply(data, .(run), model_7)
fitted.gam.model.8 = ddply(data, .(run), model_8)

##save results to table
write.csv(fitted.gam.model.1,"Gmod_1_10kav_0112.csv")
write.csv(fitted.gam.model.2,"Gmod_2_10kav_0112.csv")
write.csv(fitted.gam.model.3,"Gmod_3_10kav_0112.csv")
write.csv(fitted.gam.model.4,"Gmod_4_10kav_0112.csv")
write.csv(fitted.gam.model.5,"Gmod_5_10kav_0112.csv")
write.csv(fitted.gam.model.6,"Gmod_6_10kav_0112.csv")
write.csv(fitted.gam.model.7,"Gmod_7_10kav_0112.csv")
write.csv(fitted.gam.model.8,"Gmod_8_10kav_0112.csv")


#######################################################################################################################
##Runs GAMs on neighbourhood CV gpp (y) for 2001-12 with environmental conditions and 10km beta (X) 
## across all IBRAs using 10 sets of random points (n~8670), stratified by IBRA
setwd("C:/D_workstation/chap_1/cont_analysis/output/IBRA/overall/replicates/spatially_global/variation")

##source functions for global CV analyses
source("IBRA_functions_KCV_glob.R")

##split/apply/combine using IBRA regions as runs
fitted.gam.model.1 = ddply(data, .(run), model_1)
fitted.gam.model.2 = ddply(data, .(run), model_2)
fitted.gam.model.3 = ddply(data, .(run), model_3)
fitted.gam.model.4 = ddply(data, .(run), model_4)
fitted.gam.model.5 = ddply(data, .(run), model_5)
fitted.gam.model.6 = ddply(data, .(run), model_6)
fitted.gam.model.7 = ddply(data, .(run), model_7)
fitted.gam.model.8 = ddply(data, .(run), model_8)

##save results to table
write.csv(fitted.gam.model.1,"Gmod_1_10kcv_0112.csv")
write.csv(fitted.gam.model.2,"Gmod_2_10kcv_0112.csv")
write.csv(fitted.gam.model.3,"Gmod_3_10kcv_0112.csv")
write.csv(fitted.gam.model.4,"Gmod_4_10kcv_0112.csv")
write.csv(fitted.gam.model.5,"Gmod_5_10kcv_0112.csv")
write.csv(fitted.gam.model.6,"Gmod_6_10kcv_0112.csv")
write.csv(fitted.gam.model.7,"Gmod_7_10kcv_0112.csv")
write.csv(fitted.gam.model.8,"Gmod_8_10kcv_0112.csv")


#######################################################################################################################
##Runs GAMs on neighbourhood average gpp (y) for 2001-12 with environmental conditions and 10km beta (X) 
## within each IBRAs using 10 sets of random points (n~120/8670), stratified by IBRA
setwd("C:/D_workstation/chap_1/cont_analysis/output/IBRA/overall/replicates/10km/kernel/magnitude")

##source functions for local AV analyses
source("IBRA_functions_KAV.R")

##split/apply/combine using IBRA regions as replicates
fitted.gam.model.1 = ddply(data, .(replicate), model_1)
fitted.gam.model.2 = ddply(data, .(replicate), model_2)
fitted.gam.model.3 = ddply(data, .(replicate), model_3)
fitted.gam.model.4 = ddply(data, .(replicate), model_4)
fitted.gam.model.5 = ddply(data, .(replicate), model_5)
fitted.gam.model.6 = ddply(data, .(replicate), model_6)

##save results to table
write.csv(fitted.gam.model.1,"mod_1_10kav_0112.csv")
write.csv(fitted.gam.model.2,"mod_2_10kav_0112.csv")
write.csv(fitted.gam.model.3,"mod_3_10kav_0112.csv")
write.csv(fitted.gam.model.4,"mod_4_10kav_0112.csv")
write.csv(fitted.gam.model.5,"mod_5_10kav_0112.csv")
write.csv(fitted.gam.model.6,"mod_6_10kav_0112.csv")


#######################################################################################################################
##Runs GAMs on neighbourhood average gpp (y) for 2001-12 with environmental conditions and 10km beta (X) 
## within each IBRAs using 10 sets of random points (n~120/8670), stratified by IBRA
setwd("C:/D_workstation/chap_1/cont_analysis/output/IBRA/overall/replicates/10km/kernel/variation")

##source functions for local CV analyses
source("IBRA_functions_KCV.R")

##split/apply/combine using IBRA regions as replicates
fitted.gam.model.1 = ddply(data, .(replicate), model_1)
fitted.gam.model.2 = ddply(data, .(replicate), model_2)
fitted.gam.model.3 = ddply(data, .(replicate), model_3)
fitted.gam.model.4 = ddply(data, .(replicate), model_4)
fitted.gam.model.5 = ddply(data, .(replicate), model_5)
fitted.gam.model.6 = ddply(data, .(replicate), model_6)

##save results to table
write.csv(fitted.gam.model.1,"mod_1_10kcv_0112.csv")
write.csv(fitted.gam.model.2,"mod_2_10kcv_0112.csv")
write.csv(fitted.gam.model.3,"mod_3_10kcv_0112.csv")
write.csv(fitted.gam.model.4,"mod_4_10kcv_0112.csv")
write.csv(fitted.gam.model.5,"mod_5_10kcv_0112.csv")
write.csv(fitted.gam.model.6,"mod_6_10kcv_0112.csv")