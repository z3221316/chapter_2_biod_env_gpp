#####################################################################################################

##---------------------------------------------------#
#  +#### sample empircial GPP-ENV-BIOD matrix ####+
##---------------------------------------------------#

## create raster
n.x = ceiling(153.64-112.92)/0.2
n.y = ceiling(-10.51-(-43.64))/0.2

# index.mat = matrix(c(1:(n.x*n.y)), n.y, n.x)

index.raster = raster(nrows = n.y, ncols = n.x, 
                      xmn = 112.92, xmx = 153.64, 
                      ymn = -43.64, ymx = -10.51, 
                      vals = c(1:(n.x*n.y)))
plot(index.raster)

## create combo proability of undersampling AND number of pairs
# exp(x)
# expm1(x)

P_good = c(1 - e -n.pairs*z)
z = 0.07

x$p.good = 1-exp(-x$n_pairs*z)

## matric of BIOD data
BIOD.points.spp = data.matrix(Site_alpha_beta[1:6])
BIOD.points = data.matrix(x[1:2])

## extract block index for analyses
block.index = extract(index.raster, BIOD.points, method='simple')

test = cbind(x, block.index)

block_list = unique(block.index)

## loop over block indices
for (i.block - length(block.index)) {
  block.rows = which(test$block.index == block_list[i.block])
  block.data = test[block.rows,]
  subsample.x = x[ sample( which( x$species_count > 50 ) , 500 ) , ]
}

# dummy data
x = data.frame(species_count = runif(30000,40,70),
               LONGDEC = runif(30000,114,150),
               LATDEC = runif(30000,12,40))


# sample_alpha_rows = function (x, N) {
#   
#   # sample all rows with alpha > 50, 
#   sample.rows = which(x$species_count > 50)
# }

sample_separated_rows = function (x, alpha, dist.thresh, N) {
  # choose alpha cutoff
  sample.rows = which(x$species_count > alpha)
  # of those rows, sample N points which are > 20km apart 
  test.samples = numeric(N)
  #dist.thresh = 0.2 # don't need, put it in function call
  for (i in 1:N) {
    if (i == 1) {
      test.samples[i] = sample(sample.rows, 1)
    } else {
      new.sample = sample(sample.rows, 1)
      while ( sqrt( (x$LONGDEC[test.samples[i-1]] - x$LONGDEC[new.sample])^2 + 
                    (x$LATDEC[test.samples[i-1]] - x$LATDEC[new.sample])^2 ) < dist.thresh) {  
        new.sample = sample(sample.rows, 1)
      }
      test.samples[i] = new.sample 
      }
    }
  return(test.samples)
}

# small dist
selected.samples = sample_separated_rows(x, 50, 1, 100)
plot(x$LATDEC~x$LONGDEC)
points(x$LATDEC[selected.samples]~x$LONGDEC[selected.samples], pch=16, col="red")

# large dist - this causes the sample to jump back and forth - as you can see in the plot following 
selected.samples = sample_separated_rows(x, 50, 7, 100)
plot(x$LATDEC~x$LONGDEC)
points(x$LATDEC[selected.samples]~x$LONGDEC[selected.samples], pch=16, col="red")

# check the distances from the first point
for (i in 1:length(selected.samples)) {
  print(sqrt( (x$LONGDEC[selected.samples[1]] - x$LONGDEC[selected.samples[i+1]])^2 + 
        (x$LATDEC[selected.samples[1]] - x$LATDEC[selected.samples[i+1]])^2 ))
}

# this is less of an issue for larger geographic ranges, but somethign to think about
# even if you do 20km blocks, there is nothing stopping the points being close to each other in the corner of the blocks
# what we need is to functionalise the distance calc and run it on all previous points

# this is slow...
DistCalc = function(x, new.sample, test.samples) {
  test.samples = test.samples[!is.na(test.samples)]
  distances = numeric(length(test.samples))
  for (i in 1:length(distances)) {
    distances[i] = sqrt( (x$LONGDEC[new.sample] - x$LONGDEC[test.samples[i]])^2 + 
                           (x$LATDEC[new.sample] - x$LATDEC[test.samples[i]])^2 ) 
  }
  return(distances)
}

# this is faster
hypotenuse = function(x) {sqrt( (x[1]-x[2])^2 + (x[3]-x[4])^2 )}
DistCalc2 = function(x, new.sample, test.samples) {
  test.samples = test.samples[!is.na(test.samples)]
  lat.longs = data.frame(xi = rep(x$LONGDEC[new.sample],length(test.samples)),
                         xj = x$LONGDEC[test.samples],
                         yi = rep(x$LATDEC[new.sample],length(test.samples)),
                         yj = x$LATDEC[test.samples])
  distances = apply(X=lat.longs, MARGIN=1, FUN=hypotenuse)
  return(distances)
}

system.time(DistCalc(x, 1, 1:100000))
system.time(DistCalc2(x, 1, 1:100000))

SampleMinimumDist = function (x, alpha, dist.thresh, N) {
  # choose alpha cutoff
  sample.rows = which(x$species_count > alpha)
  # of those rows, sample N points which are > 20km apart 
  test.samples = as.numeric(rep(NA,N))
  #dist.thresh = 0.2 # don't need, put it in function call
  for (i in 1:N) {
    if (i == 1) {
      test.samples[i] = sample(sample.rows, 1)
    } else {
      new.sample = sample(sample.rows, 1)
      while ( min(DistCalc2(x, new.sample, test.samples)) < dist.thresh) {  
        new.sample = sample(sample.rows, 1)
      }
      test.samples[i] = new.sample
    }
  }
  return(test.samples)
}

# not lets look at the differences
par(mfrow=c(1,3))

plot(x$LATDEC~x$LONGDEC, type="n")
rows = sample(1:nrow(x),300)
points(x$LATDEC[rows]~x$LONGDEC[rows], pch=16, col="black")

plot(x$LATDEC~x$LONGDEC, type="n")
selected.samples = sample_separated_rows(x, 50, 1, 300)
points(x$LATDEC[selected.samples]~x$LONGDEC[selected.samples], pch=16, col="red")

plot(x$LATDEC~x$LONGDEC, type="n")
selected.samples = SampleMinimumDist(x, 50, 1, 300)
points(x$LATDEC[selected.samples]~x$LONGDEC[selected.samples], pch=16, col="blue")





